import React, { Component } from 'react';
import { BlockPicker } from 'react-color'
import { ipcRenderer } from 'electron';

import styles from './Home.css';

type Props = {};

export default class Home extends Component<Props> {
  props: Props;

  state = {
     background: '#000000',
  }

  handleChangeComplete = (color) => {
    ipcRenderer.send('colorChange', color.hex)
    this.setState({ background: color.hex });
  }

  render() {
    const { background } = this.state
    const colors = [
      '#000000',
      '#2CCCE4',
      '#37D67A',
      '#555555',
      '#808080',
      '#A9A9A9',
      '#D3D3D3',
      '#dce775',
      '#ffffff',
    ]
    return (
      <div className={styles.container} data-tid="container">
        <BlockPicker color={ background } colors={colors} onChangeComplete={ this.handleChangeComplete } width='100%' height='100%' />
      </div>
    );
  }
}
