/* eslint global-require: off */

import { ipcMain } from 'electron';

const { exec } = require('child_process')
const fs = require('fs')
const menubar = require('menubar')
const path = require('path')

const dir = path.join(__dirname)

const mb = menubar({
  alwaysOnTop: process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true',
  dir,
  preloadWindow: true,
  frame: false,
  transparent: true,
  resizable: false,
  width: 315,
  height: 200,
})

if (process.env.NODE_ENV === 'development' || process.env.DEBUG_PROD === 'true') {
  require('electron-debug')();
}

mb.on('ready', () => {
  ipcMain.on('colorChange', (event, arg) => {
    const colorHex = arg.slice(1)
    const red = colorHex.slice(0, 2)
    const green = colorHex.slice(2, 4)
    const blue = colorHex.slice(4, 6)
    const device = process.env.PICKE_DEVICE || '/dev/cu.usbmodem2238770'

    if (fs.existsSync(device)) {
      // echo -n "01 63 ff 00 00 00 00 00" | xxd -r -p > /dev/cu.usbmodem2238771
      const command = `echo -n "01 63 ${red} ${green} ${blue} 00 00 00" | xxd -r -p > ${device}`
      console.log({command})
      exec(command)
    }
  })
});


